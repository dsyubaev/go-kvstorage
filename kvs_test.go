package main

import (
	"context"
	"testing"
)

func get_storage(ctx context.Context) MyStorage {
	var s MyStorage
	s.Put(ctx, "a", 1)
	s.Put(ctx, "b", 2)
	s.Put(ctx, "c", 3)
	return s
}

func TestGet(t *testing.T) {
	ctx := context.Background()
	s := get_storage(ctx)
	want := 1
	msg, err := s.Get(ctx, "a")
	if want != msg || err != nil {
		t.Fatalf(`Get("a") = %q, %v, want match for %#q, nil`, msg, err, want)
	}
}

func TestGetNotFound(t *testing.T) {
	ctx := context.Background()
	s := get_storage(ctx)
	msg, err := s.Get(ctx, "aa")
	if msg != 0 || err == nil {
		t.Fatalf(`Get("aa") = %q, %v, want 0, error`, msg, err)
	}
}

func TestDelete(t *testing.T) {
	ctx := context.Background()
	s := get_storage(ctx)

	s.Delete(ctx, "a")
	msg, err := s.Get(ctx, "a")
	if msg != 0 || err == nil {
		t.Fatalf(`Get("aa") = %q, %v, want 0, error`, msg, err)
	}
}
