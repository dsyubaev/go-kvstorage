package main

/*

- без использования готовых решений
- с использованием стандартной библиотеки Go [sync] (https://pkg.go.dev/sync)
- приложите ссылки на код gitlab или github
- предоставьте примеры использования KVStorage с tcp или http-сервером
*/

import (
	"context"
	"errors"
	"sync"
)

type KVStorage interface {
	Get(ctx context.Context, key string) (interface{}, error)
	Put(ctx context.Context, key string, val interface{}) error
	Delete(ctx context.Context, key string) error
}

type MyStorage struct{ m sync.Map }

func (storage *MyStorage) Get(ctx context.Context, key string) (interface{}, error) {
	value, ok := storage.m.Load(key)
	if ok {
		return value, nil
	}
	return 0, errors.New("key not found")
}

func (storage *MyStorage) Put(ctx context.Context, key string, val interface{}) {
	storage.m.Store(key, val)
}

func (storage *MyStorage) Delete(ctx context.Context, key string) {
	storage.m.Delete(key)
}
